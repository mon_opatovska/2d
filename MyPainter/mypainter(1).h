#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QElapsedTimer>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();

	int red_v, green_v, blue_v, red_h, green_h, blue_h;

public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void ActionNegative();
	void ActionBlackWhite();
	void ActionSepiaTone();
	void ActionMedian();
	void ActionSaltPepper();
	void ActionLeft();
	void ActionRight();
	void Klik_ok();


private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;

};

#endif // MYPAINTER_H
