#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>


class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void fastNegative();
	void blackWhite();
	void sepiaTone();
	void medianFilter();
	void saltPepper();
	void RotateLeft();
	void RotateRight();

	void Hranica();
	void Scan_line(int red_v, int green_v, int blue_v);
	void sort();
	void TH();
	void DDA(QPainter *painter, int x1, int x2, int y1, int y2);
	void Skalovanie(double parameter);
	void Otacanie(bool smer);
	void Preklopenie(bool os);
	void Skosenie(bool os);
	void Bezier();
	void Bspline();
	void Midpoint();
	void Vypln_kruznice(int red_v, int green_v, int blue_v);

	QVector <QPoint> body;
	QVector <QVector <double>>  tabulkaHran;
	QVector <QVector <double>> TAH;
	QVector <double> vektor;

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }


	public slots:
	void clearImage();

	
protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;

	
};

#endif // PAINTWIDGET_H
