#include "paintwidget.h"
#include <math.h>


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x, y, QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName, "png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::fastNegative()
{
	int x = image.height();
	int y = image.width();
	unsigned __int64 asd = 0x00ffffff00ffffff;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		QRgb* scanline = (QRgb*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			scanline[j] = 16777215 - scanline[j];
		}
		/*unsigned __int64* scanline=(unsigned __int64*)image.scanLine(i);
		for (int j = 0; j < y/2; j++)
		{
		scanline[j] ^= asd;
		}*/
	}
	update();
}

void PaintWidget::blackWhite()
{

	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			uchar color = (scanline[j * 4 + 0] * 0.11) + (scanline[j * 4 + 1] * 0.59) + (scanline[j * 4 + 2] * 0.3);
			scanline[j * 4 + 0] = color;
			scanline[j * 4 + 1] = color;
			scanline[j * 4 + 2] = color;
		}
	}
	/*for (int i = 0; i < image.width(); i++)
	{
	for (int j = 0; j < image.height(); j++)
	{
	QColor farba = image.pixelColor(i, j);
	farba.setRed(255 - farba.red());
	farba.setBlue(255 - farba.blue());
	farba.setGreen(255 - farba.green());
	image.setPixelColor(i, j, farba);
	}
	}*/
	update();
}

void PaintWidget::sepiaTone()
{
	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			int colorR = scanline[j * 4 + 2] * 0.393 + scanline[j * 4 + 1] * 0.769 + scanline[j * 4 + 0] * 0.189;
			int colorG = scanline[j * 4 + 2] * 0.349 + scanline[j * 4 + 1] * 0.686 + scanline[j * 4 + 0] * 0.168;
			int colorB = scanline[j * 4 + 2] * 0.272 + scanline[j * 4 + 1] * 0.534 + scanline[j * 4 + 0] * 0.131;
			scanline[j * 4 + 2] = colorR > 255 ? 255 : colorR;
			scanline[j * 4 + 1] = colorG > 255 ? 255 : colorG;
			scanline[j * 4 + 0] = colorB > 255 ? 255 : colorB;
		}
	}
	/*
	outputRed = (inputRed * .393) + (inputGreen *.769) + (inputBlue * .189)
	outputGreen = (inputRed * .349) + (inputGreen *.686) + (inputBlue * .168)
	outputBlue = (inputRed * .272) + (inputGreen *.534) + (inputBlue * .131)*/
	/*for (int i = 0; i < x; i++)
	{
	for (int j = 0; j < y; j++)
	{
	QRgb farba = image.pixel(j, i);
	image.setPixel(j, i, 16777215 - farba);
	}
	}*/
	update();
}

void PaintWidget::medianFilter()
{

	int x = image.height();
	int y = image.width();
	uchar *data = image.bits();
	int riadok = image.bytesPerLine();
#pragma omp parallel
	{
		int reds[9];
		int greens[9];
		int blues[9];
#pragma omp for
		for (int i = 1; i < x - 1; i++)
		{
			for (int j = 1; j < y - 1; j++)
			{
				int counter = 0;
				for (int k = -1; k < 2; k++)
					for (int l = -1; l < 2; l++)
					{
						blues[counter] = data[(i + k)*riadok + (j + l) * 4 + 0];
						greens[counter] = data[(i + k)*riadok + (j + l) * 4 + 1];
						reds[counter] = data[(i + k)*riadok + (j + l) * 4 + 2];
						counter++;
					}
				data[i*riadok + j * 4 + 0] = blues[selectKth(blues, 0, 9, 4)];
				data[i*riadok + j * 4 + 1] = greens[selectKth(greens, 0, 9, 4)];
				data[i*riadok + j * 4 + 2] = reds[selectKth(reds, 0, 9, 4)];
			}
		}
	}
}

void PaintWidget::saltPepper()
{
	int count = image.width()*image.height()*0.1;
	QRgb *data = (QRgb *)image.bits();
	int riadok = image.bytesPerLine() / 4;
#pragma omp parallel
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> xsur(0, image.width() - 1);
		std::uniform_int_distribution<> ysur(0, image.height() - 1);
#pragma omp for
		for (int i = 0; i < count; i++)
		{
			data[ysur(gen)*riadok + xsur(gen)] = i % 2 == 0 ? 0x00000000 : 0x00ffffff;
		}
	}
}

void PaintWidget::RotateLeft()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[i + (y - j - 1)*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::RotateRight()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[x - i - 1 + j*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		lastPoint = event->pos();
		painting = true;
		QPainter painter(&image);
		painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(lastPoint);
		body.append(lastPoint);
		update();
	}
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());


	if (event->buttons() & Qt::RightButton)
	{
		int prvyx = body[0].x();
		int prvyy = body[0].y();

		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x() - (prvyx - event->pos().x()), body[i].y() - (prvyy - event->pos().y())));
		}

		this->clearImage();
		this->Hranica();
	}

}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) 
	{
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	//QPainter painter(&image);

//	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//painter.drawLine(lastPoint, endPoint);
	//painter.drawPoint(lastPoint);
	//painter.drawPoint(endPoint);
	//modified = true;

	//int rad = (myPenWidth / 2) + 2;
	//update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));

	//lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

void PaintWidget::DDA(QPainter *painter, int x1, int y1, int x2, int y2)
{
	int xx, yy;
	double m;

	if (x2 < x1)
	{
		xx = x2;
		x2 = x1;
		x1 = xx;
		yy = y2;
		y2 = y1;
		y1 = yy;
	}

	double x = (double)x1;
	double y = (double)y1;

	int dx = fabs(x2 - x1);
	int dy = fabs(y2 - y1);

	if (dx >= dy)
		m = (double)dy / (double)dx;

	else
		m = (double)dx / (double)dy;

	do
	{
		if (y1 > y2)
		{
			if (dy < dx)
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + 1;
				y = y - m;
			}

			else
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + m;
				y = y - 1;
			}
		}

		else
		{
			if (dy < dx)
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + 1;
				y = y + m;
			}

			else
			{
				painter->drawPoint((int)round(x), (int)round(y));
				x = x + m;
				y = y + 1;
			}
		}
	} while ((int)round(x) != x2 || (int)round(y) != y2);

	painter->drawPoint(x2, y2);
}

void PaintWidget::Hranica()
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(myPenColor));

	for (int i = 0; i < body.size() - 1; i++)
		this->DDA(&painter, body.at(i).x(), body.at(i).y(), body.at(i + 1).x(), body.at(i + 1).y());

	this->DDA(&painter, body.at(0).x(), body.at(0).y(), body.at(body.size() - 1).x(), body.at(body.size() - 1).y());
	
}

void PaintWidget::Skalovanie(double parameter)
{
	for (int i = 0; i < body.size(); i++)
	{
		body.replace(i, QPoint(body[0].x() + (int)round(((double)body[i].x() - (double)body[0].x()) * parameter),
								body[0].y() + (int)round(((double)body[i].y() - (double)body[0].y()) * parameter)));
	}

	this->clearImage();
	
	this->Hranica();
}

void PaintWidget::Otacanie(bool smer)
{
	double uhol = M_PI / 12.;
	
	if (smer == 0)
	{
		for (int i = 0; i < body.size(); i++)
		{
			double dlzkax = (double)body[i].x() - (double)body[0].x();
			double dlzkay = (double)body[i].y() - (double)body[0].y();

			body.replace(i, QPoint((int)round((dlzkax * cos(uhol)) - dlzkay * sin(uhol)) + body[0].x(),
				body[0].y() + (int)round((dlzkax * sin(uhol)) + dlzkay * cos(uhol))));
		}
	}

	if (smer == 1)
	{
		for (int i = 0; i < body.size(); i++)
		{
			double dlzkax = (double)body[i].x() - (double)body[0].x();
			double dlzkay = (double)body[i].y() - (double)body[0].y();

			body.replace(i, QPoint((int)round((dlzkax * cos(uhol)) + dlzkay * sin(uhol)) + body[0].x(),
				body[0].y() + (int)round((-dlzkax * sin(uhol)) + dlzkay * cos(uhol))));
		}
		
	}
	
	this->clearImage();

	this->Hranica();
}

void PaintWidget::Preklopenie(bool os)
{
	if (os == 0)
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x() + 2 * (body[0].x() - body[i].x()), body[i].y()));
		}
	}

	if (os == 1)
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x(), body[i].y() + 2 * (body[0].y() - body[i].y())));
		}
	}
	
	this->clearImage();

	this->Hranica();
}

void PaintWidget::Skosenie(bool os)
{
	double d = 0.5;
	
	if (os == 1) //os x
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x() - d * (body[0].y() - body[i].y()), body[i].y()));
		}
	}

	if (os == 0) // os y
	{
		for (int i = 0; i < body.size(); i++)
		{
			body.replace(i, QPoint(body[i].x(), body[i].y() + d * (body[i].x() - body[0].x())));
		}
	}

	this->clearImage();

	this->Hranica();
}

void PaintWidget::Bezier()
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	
	int delta = 1000;
	int pocet_bodov = body.size();
	float t = 0;
	QPointF B1;
	std::vector<QPointF> P1, P2;

	if (pocet_bodov < 2)
		return;

	P1.resize(pocet_bodov);
	P2.resize(pocet_bodov);

	for (int i = 0; i < pocet_bodov; i++) //priradenie povodnych bodov do vektora
		P1[i] = (QPointF)body[i];

	B1 = P1[0];

	while (t < 1)
	{
		t = t + 1.0 / delta;

		for (int j = 1; j < pocet_bodov; j++)
		{
			for (int i = 0; i < pocet_bodov - j; i++)
				P2[i] = (1 - t) * P1[i] + t *  P1[i + 1];
			
			P1 = P2;
		}

		painter.drawLine(B1, P1[0]);

		B1 = P1[0];
	
		for (int i = 0; i < pocet_bodov; i++) 
			P1[i] = (QPointF)body[i];
			
	}

	P1.clear();
	P2.clear();
}

void PaintWidget::Bspline()
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	
	
	int delta = 1000;
	int pocet_bodov = body.size();
	float t = 0;
	float bod0, bod1, bod2, bod3;
	QPointF B1, B2;
	

	if (pocet_bodov < 4)//musia byt aspon 4 body
		return;

	B1 = body[0];

	for (int i = 0; i < body.size() - 4; i++)
	{
		while (t < 1)
		//for (int delenie = 0; delenie <= delta; delenie++)
		{
			
			//t = delenie * (1.0 / delta);
			t = t + 1.0 / delta;

			//B1 = B2;

			printf("%d %d\n", B1.x(), B1.y());

			//vypocet bodov casti krivky
			bod0 = -((t * t * t) / 6.0) + ((t * t) / 2.0) - (t / 2.0) + (1 / 6.0);
			bod1 = ((t * t * t) / 2.0) - (t * t) + (2 / 3.0);
			bod2 = -((t * t * t) / 2.0) + ((t * t) / 2.0) + (t / 2.0) + (1 / 6.0);
			bod3 = (t * t * t) / 6.0;

			B2.setX((body[i].x() * bod0) + (body[i + 1].x() * bod1) + (body[i + 2].x() * bod2) + (body[i + 3].x() * bod3));
			B2.setY((body[i].y() * bod0) + (body[i + 1].y() * bod1) + (body[i + 2].y() * bod2) + (body[i + 3].y() * bod3));

		//	if (t == 0 && i == 0) 
			B1 = B2;
			printf("%d %d\n", B2.x(), B2.y());
			painter.drawLine(B1, B2);
		}
	}
}

void PaintWidget::Midpoint()
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	int xs, ys, r, p1, x, y, dvax, dvay;

	if (body.size() != 2)
		return;

	xs = body[0].x();
	ys = body[0].y();
	r = (int)(abs(sqrt((body[1].x() - body[0].x()) * (body[1].x() - body[0].x()) +
		               (body[1].y() - body[0].y()) * (body[1].y() - body[0].y()))));
	p1 = 1 - r;
	x = 0;
	y = r;
	dvax = 3;
	dvay = 2 * r - 2;

	while (x <= y)
	{
		painter.drawPoint(x + xs, y + ys); //vsetky sumerne body
		painter.drawPoint(y + xs, x + ys);
		painter.drawPoint(y + xs, -x + ys);
		painter.drawPoint(x + xs, -y + ys);
		painter.drawPoint(-x + xs, -y + ys);
		painter.drawPoint(-y + xs, -x + ys);
		painter.drawPoint(-y + xs, x + ys);
		painter.drawPoint(-x + xs, y + ys);

		if (p1 > 0) 
		{
			p1 = p1 - dvay;
			dvay = dvay - 2;
			y--;
		}

		p1 = p1 + dvax;
		x++;
		dvax = dvax + 2;
	}
}

void PaintWidget::Vypln_kruznice(int red_v, int green_v, int blue_v)
{
	QPainter painter(&image);
	painter.setPen(QPen(QColor(red_v, green_v, blue_v), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	int xs, ys, r, dx, dy;

	if (body.size() != 2)
		return;

	xs = body[0].x();
	ys = body[0].y();
	r = (int)(abs(sqrt((body[1].x() - body[0].x()) * (body[1].x() - body[0].x()) +
		(body[1].y() - body[0].y()) * (body[1].y() - body[0].y()))));

	
	for (int i = 0; i < 2 * r; i++) 
	{
		for (int j = 0; j < 2 * r; j++) 
		{
			dx = r - i;
			dy = r - j;

		    if ((dx * dx + dy * dy) <= (r * r)) 
				painter.drawPoint(xs + dx, ys + dy);
		}
	}
}

void PaintWidget::sort()
{
	for (int i = 0; i < tabulkaHran.size() - 1; i++) //triedenie podla yz
	{
		QVector <double> pom;

		for (int k = 0; k < tabulkaHran.size() - 1; k++)
		{
			if (tabulkaHran.at(k + 1).at(1) < tabulkaHran.at(k).at(1))
			{
				pom = tabulkaHran[k + 1];
				tabulkaHran[k + 1] = tabulkaHran[k];
				tabulkaHran[k] = pom;
			}
		}
	}

	for (int i = 0; i < tabulkaHran.size() - 1; i++) //triedenie podla xz
	{
		QVector <double> pom;

		for (int k = 0; k < tabulkaHran.size() - 1; k++)
		{
			if ((tabulkaHran.at(k + 1).at(1) == tabulkaHran.at(k).at(1)) && (tabulkaHran.at(k + 1).at(0) < (tabulkaHran.at(k).at(0))))
			{
				pom = tabulkaHran[k + 1];
				tabulkaHran[k + 1] = tabulkaHran[k];
				tabulkaHran[k] = pom;
			}
		}
	}

	for (int i = 0; i < tabulkaHran.size() - 1; i++) //triedenie podla w
	{
		QVector <double> pom;

		for (int k = 0; k < tabulkaHran.size() - 1; k++)
		{
			if ((((tabulkaHran.at(k + 1).at(1) == tabulkaHran.at(k).at(1)) && (tabulkaHran.at(k + 1).at(0) == (tabulkaHran.at(k).at(0)))))
				&& (tabulkaHran.at(k + 1).at(3) < (tabulkaHran.at(k).at(3))))
			{
				pom = tabulkaHran[k + 1];
				tabulkaHran[k + 1] = tabulkaHran[k];
				tabulkaHran[k] = pom;
			}
		}
	}
}

void PaintWidget::TH()
{
	tabulkaHran.clear();
	
	double w, x1, x2, y1, y2, dx, dy;
	for (int i = 0; i < body.size(); i++)
	{
		if (i != (body.size() - 1))
		{
			x1 = body[i].x();
			y1 = body[i].y();
			x2 = body[i + 1].x();
			y2 = body[i + 1].y();
		}

		else //posledny bod
		{
			x1 = body[body.size() - 1].x();
			y1 = body[body.size() - 1].y();
			x2 = body[0].x();
			y2 = body[0].y();
		}

	
		if ((y2 - y1) != 0)
		{
			if (y1 > y2)
			{
				std::swap(x1, x2);
				std::swap(y1, y2);
			}
			dy = y2 - y1;
			w = (x2 - x1) / dy;
			vektor.push_back(x1);
			vektor.push_back(y1);
			vektor.push_back(y2 - 1);
			vektor.push_back(w);
			tabulkaHran.push_back(vektor);
			vektor.clear();
		}
	}

	printf("hrany\n x1   y1   y2   w\n");
	for (int i = 0; i < tabulkaHran.size(); i++)
	{
		for (int j = 0; j < 4; j++)
		{
			printf("%.2lf ", tabulkaHran[i][j]);
		}
		printf("\n");
	}
}

void PaintWidget::Scan_line(int red_v, int green_v, int blue_v)
{
	QPainter painter(&image);
	painter.setPen(QPen(QColor(red_v, green_v, blue_v), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	QVector<double> vekt;
	
	this->TH();
	this->sort();
	
	int ya, ymax, ymin;
		
	ymin = tabulkaHran[0][1];
	ymax = tabulkaHran[(body.size() - 1)][2];

	printf("sort\n");
	for (int i = 0; i < tabulkaHran.size(); i++)
	{
		for (int j = 0; j < 4 ; j++)
			printf("%.2lf ", tabulkaHran[i][j]);

		printf("\n");
	}

	printf("%d %d\n", ymin, ymax);
	
	for (int ya = ymin; ya <= ymax; ya++)
	{	
		//vytvorenie TAH
		for (int i = 0; i < body.size(); i++)
		{
			if (ya == tabulkaHran[i][1])
			{
				vekt.push_back(tabulkaHran[i][0]);
				vekt.push_back(tabulkaHran[i][1]);
				vekt.push_back(tabulkaHran[i][2]);
				vekt.push_back(tabulkaHran[i][3]);
				TAH.push_back(vekt);
				vekt.clear();
			}
		}	
	}

	for (int i = 0; i < TAH.size(); i++)
	{
		for (int j = 0; j < 4; j++)
			printf("%.2lf ", TAH[i][j]);

		printf("\n");
	}
	
	//zoradenie podla x
	for (int i = 0; i < TAH.size(); i++)
	{
		for (int j = 0; j < TAH.size(); j++)
		{
			if (TAH[i][0] < TAH[j][0])
			{
				QVector<double> tmp = TAH[i];
				TAH[i] = TAH[j];
				TAH[j] = tmp;
			}
		}
	}

	printf("usporiadana\n");
	for (int i = 0; i < TAH.size(); i++)
	{
		for (int j = 0; j < 4; j++)
			printf("%.2lf ", TAH[i][j]);

		printf("\n");
	}
	
	//vypocet priesecnikov
	QVector<int>priesecniky;
	priesecniky.clear();
	for (int i = 0; i < TAH.size(); i = i + 2)
	{
		int x1 = 0, x2 = 0;
		//w*ya - w*yz + xz
		x1 = TAH[i][3] * ya - TAH[i][3] * TAH[i][1] + TAH[i][0];
		x2 = TAH[i + 1][3] * ya - TAH[i + 1][3] * TAH[i + 1][1] + TAH[i + 1][0];
		priesecniky.push_back(x1);
		priesecniky.push_back(x2);
	}

	for (int i = 0; i < priesecniky.size(); i++)
	{
		printf("priesecnik %d\n", priesecniky[i]);
	}
	
	qSort(priesecniky.begin(), priesecniky.end());
	for (int i = 0; i < priesecniky.size(); i = i + 2) 
	{
		painter.drawLine(priesecniky[i], ya, priesecniky[i + 1], ya);
	}
	
	//vymazanie hrany z TAH
	for (int i = 0; i < TAH.size(); i++)
	{
		if (TAH[i][2] == ya)
		{
			TAH.removeAt(i);
		}
	}
}