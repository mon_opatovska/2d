#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(1000, 415);
}

void MyPainter::ActionNegative()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.fastNegative();
	QString text= "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionBlackWhite()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.blackWhite();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionSepiaTone()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.sepiaTone();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionMedian()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.medianFilter();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionSaltPepper()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.saltPepper();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionLeft()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.RotateLeft();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionRight()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.RotateRight();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::Klik_ok()
{
	paintWidget.newImage(1000, 415);

	red_h = ui.spinBox->value();
	green_h = ui.spinBox_2->value();
	blue_h = ui.spinBox_3->value();
	red_v = ui.spinBox_4->value();
	green_v = ui.spinBox_5->value();
	blue_v = ui.spinBox_6->value();

	paintWidget.Hranica(red_h, green_h, blue_h);
	paintWidget.Scan_line(red_v, green_v, blue_v);
}
